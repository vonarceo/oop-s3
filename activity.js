// What is the blueprint where objects are created from?

// CLASS

// What is the naming convention applied to classes?

// First Letter Capitalize, must be Singular

// What keyword do we use to create objects from a class?

// NEW

// What is the technical term for creating an object from a class?

// INSTANTIATE

// What class method dictates HOW objects will be created from that class?

// CONSTRUCTOR


// ACTIVITY 2

/*

1. Should class methods be included in the class constructor?

- Yes

2. Can class methods be separated by commas?

- no, ; (semicolons)

3. Can we update an object’s properties via dot notation?

- Yes 

4. What do you call the methods used to regulate access to an object’s properties?

- Setter & Getter

5. What does a method need to return in order for it to be chainable?

- return this;

*/

class Student{
	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
		if(typeof(grades) === "object" && grades.length === 4){
            (grades.every(num => (num >= 0 && num <= 100)))? this.grades = grades : this.grades = "undefined"
        }
        else this.grades = "undefined";
	}

	login(){
		console.log(`${this.email} has logged in`)
		return this;
	}

	logout(){
		console.log(`${this.email} has logged out`)
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
		return this;
	}
	computeAve(){
	    let sum = 0;
	    this.grades.forEach(grade => sum = sum + grade);
	    // update the gradeAve property
	    this.gradeAve = sum/4;
	    return this;
	}
	willPass() {
	    this.passed = this.gradeAve >= 85 ? true : false;
	    return this;
	}
	willPassWithHonors() {
		if(this.passed == true) {
			if(this.gradeAve >= 90) {
				this.passedWithHonors = true;

			}
			else {
				this.passedWithHonors = false
			}
		} 
		else {
			this.passedWithHonors = undefined;
		}

		return this
	    }

}	



let studentOne = new Student("John", "john@mail.com", [72, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);
	
let testStundent = new Student("Von", "von@email.com", [75,75,75,"Hello"])





