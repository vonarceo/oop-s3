console.log("Von Arceo")

// we define classes with the use of the keyword "class" and a set of {}. naming convention dictates that class names should begin with an uppercase letter.


// consttructor keyword allow us to pass argument when we instantiate object from classes

class Student{
	
	constructor(name, email){
		this.name = name;
		this.email = email;
	}
}

let studentOne = new Student("John", "john@email.com");
let studentTwo = new Student("Jane", "jane@email.com");

